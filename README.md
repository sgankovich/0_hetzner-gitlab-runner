#11 GitLab Runner on Hetzner (Shell runner with a terraform application onboard)


After deployment you will have a shell-runner with terraform and ansible applications on it. It will be created on the Hetzner Cloud and joined to your account.

## Installation

Fork this repository to your account

## Usage

Set  variables in the repository settings:
```
GITLAB_REG_TOKEN as a variable
HETZNER_TOKEN as a variable
SSH_PRIVATE_KEY as a file

```
Also change some settings in the variables.tf like a type of VM, a terraform version, if you want.

## License
[MIT](https://choosealicense.com/licenses/mit/)
