#initial connect to Hetzner
provider "hcloud" {
  token = var.Htoken
}

# Create Ubuntu-server
resource "random_id" "id" {
	  byte_length = 8
}

resource "hcloud_server" "gitlab_runner" {
  name = "${var.vm_name}-${random_id.id.hex}"
  image = var.vm_image
  server_type = var.vm_type
  ssh_keys = [var.ssh_key_name]

  provisioner "remote-exec" {
    inline = [
      "yum install epel-release -y",
      "yum update -y",
      "yum install ansible -y",
      "yum install wget unzip -y",
      "wget https://releases.hashicorp.com/terraform/${var.terraform_ver}/terraform_${var.terraform_ver}_linux_amd64.zip",
      "unzip terraform_${var.terraform_ver}_linux_amd64.zip && rm -f terraform_${var.terraform_ver}_freebsd_amd64.zip",
      "mv terraform /usr/local/bin/terraform",
      "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash",
      "yum install gitlab-runner -y",
      "gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token ${var.gitlab_reg_token} --executor 'shell' --description 'shell-runner' --tag-list 'shell, terraform, ansible' --run-untagged='true' --locked='false' --access-level='not_protected'"
     ]
    connection {
      type        = "ssh"
      user        = var.vm_user_name
      private_key = file(var.ssh_private_key_path)
      host        = self.ipv4_address
    }
  }

}




 
