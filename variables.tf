#token for connecting to Hetzner
variable "Htoken" {
  type        = string
}
#ssh-key name
variable "ssh_key_name" {
  type        = string
  default = "ubuntu@ubuntu1804"
}

variable "vm_user_name" {
  type        = string
  default = "root"
}

variable "vm_name" {
  type        = string
  default = "gitlab-runner"
}

variable "vm_image" {
  type        = string
  default = "centos-8"
}

variable "vm_type" {
  type        = string
  default = "cx11"
}

variable "ssh_private_key_path" {
  type        = string
  default = "ssh_key.pem"
}

variable "terraform_ver" {
  type        = string
  default = "0.13.1"
}

variable "gitlab_reg_token" {
  type        = string
}

